import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AddAnimalComponent } from './add-animal/add-animal.component';
import { AnimalComponent } from './animal/animal.component';
import { EditAnimalComponent } from './edit-animal/edit-animal.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'animal', component: AnimalComponent},
  {path: 'animal/add', component: AddAnimalComponent},
  {path: 'animal/edit/:id', component: EditAnimalComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
