import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Emitters} from '../emitters/emitters';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  message = '';
  valueFromServer = ''
  constructor(
    private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    let localToken = localStorage.getItem('token')
    localToken?.slice(1)
    let XD = localToken?.slice(1, -1)
    var header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${XD}`)
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    }
    this.http.get('https://localhost:44363/api/User', header).subscribe(
      (res: any) => {
        console.log(res)
        this.message = `Hi ${res.email}`;
        Emitters.authEmitter.emit(true);
      },
      err => {
        this.message = 'You are not logged in';
        Emitters.authEmitter.emit(false);
      }
    );
  }

}
