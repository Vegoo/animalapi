export interface IAnimal {
    id: number;
    name: string;
    dateOfBirth: Date;
}