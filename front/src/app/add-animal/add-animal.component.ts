import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.css']
})
export class AddAnimalComponent implements OnInit {
  form!: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: "",
      dateOfBirth: ""
    });
  }

  submit(): void {
    let localToken = localStorage.getItem('token')
    localToken?.slice(1)
    let XD = localToken?.slice(1, -1)
    console.log(XD)
    var header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${XD}`)
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    }
    this.http.post('https://localhost:44363/api/Pet', this.form.getRawValue(), header)
      .subscribe(() => this.router.navigate(['/animal']));
  }

}
