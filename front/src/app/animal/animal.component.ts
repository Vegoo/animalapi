import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Subscription } from 'rxjs/internal/Subscription';
import { IAnimal } from '../models/animal';
import { AnimalService } from '../services/animal.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {
  sub!: Subscription;

  animals: IAnimal[] = [];


  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private animalService: AnimalService
  ) {
  }
  
  ngOnInit(): void {
    this.sub = this.animalService.getMyAnimals().subscribe({
      next: animals => {
        this.animals = animals;
      }
    })
  }

  delete(id: number){
    let localToken = localStorage.getItem('token')
    localToken?.slice(1)
    let XD = localToken?.slice(1, -1)
    console.log(XD)
    var header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${XD}`)
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    }
    this.http.delete(`https://localhost:44363/api/Pet/${id}`, header)
      .subscribe(() => this.router.navigate(['/']));
  }
}