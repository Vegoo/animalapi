import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-animal',
  templateUrl: './edit-animal.component.html',
  styleUrls: ['./edit-animal.component.css']
})
export class EditAnimalComponent implements OnInit {
  form!: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  public id!: any;




  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.form = this.formBuilder.group({
      name: "",
      dateOfBirth: ""
    });
  }

  submit(): void {
    let localToken = localStorage.getItem('token')
    localToken?.slice(1)
    let XD = localToken?.slice(1, -1)
    console.log(XD)
    var header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${XD}`)
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    }
    this.http.put(`https://localhost:44363/api/Pet/${this.id}`, this.form.getRawValue(), header)
      .subscribe(() => this.router.navigate(['/']));
  }
}
