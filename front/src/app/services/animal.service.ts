import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { IAnimal } from '../models/animal';

@Injectable({
  providedIn: 'root'
})

export class AnimalService {

  constructor(private http: HttpClient) { }

  getMyAnimals(): Observable<IAnimal[]> {
    let localToken = localStorage.getItem('token')
    localToken?.slice(1)
    let XD = localToken?.slice(1, -1)
    var header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${XD}`)
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    }
    return this.http.get<IAnimal[]>('https://localhost:44363/api/User/mypets', header);
  }
}