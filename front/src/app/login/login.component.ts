import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import { IUser } from '../models/user';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  submit(): void {
    //console.log(this.form.getRawValue())
    this.http.post<IUser>('https://localhost:44363/api/User/login', this.form.getRawValue())
      .pipe(map(user => {
        if(user && user.token) {
          localStorage.setItem('token', JSON.stringify(user.token))
        }
        this.router.navigate(["/"])
        return user;
      }))
      .subscribe()
  }
}
