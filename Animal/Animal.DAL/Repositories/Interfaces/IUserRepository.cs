﻿using Animal.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Animal.DAL.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task AddUser(User newUser);
        public Task<User> GetByEmail(string email);
        public Task<User> GetById(int id);
        public Task<List<Pet>> GetMyPets(int id);
    }
}
