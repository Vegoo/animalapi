﻿using Animal.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal.DAL.Repositories.Interfaces
{
    public interface IPetRepository
    {
        public Task CreateNewPet(Pet pet);
        public Task<Pet> GetById(int id);
        void DeletePet(Pet pet);
        public Task UpdatePet();
        public Task<List<Pet>> GetAll();
    }
}
