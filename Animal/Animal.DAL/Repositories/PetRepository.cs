﻿using Animal.DAL.Entities;
using Animal.DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal.DAL.Repositories
{
    public class PetRepository : IPetRepository
    {
        private readonly AppDbContext _context;

        public PetRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task CreateNewPet(Pet pet)
        {
            await _context.Pets.AddAsync(pet);
            await _context.SaveChangesAsync();
        }

        public void DeletePet(Pet pet)
        {
            _context.Pets.Remove(pet);
            _context.SaveChanges();
        }

        public async Task<List<Pet>> GetAll()
        {
            var results = await _context.Pets.ToListAsync();
            return results;
        }

        public async Task<Pet> GetById(int id)
        {
            var result = await _context.Pets.FirstOrDefaultAsync(p => p.Id == id);
            return result;
        }

        public async Task UpdatePet()
        {
            await _context.SaveChangesAsync();
        }
    }
}
