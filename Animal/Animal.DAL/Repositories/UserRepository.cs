﻿using Animal.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Animal.DAL.Repositories.Interfaces
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _context;

        public UserRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task AddUser(User newUser)
        {
            await _context.Users.AddAsync(newUser);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetByEmail(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<User> GetById(int id)
        {
            return await _context
                .Users
                .Include(u => u.Pets)
                .FirstOrDefaultAsync(u => u.Id == id);
                
        }

        public async Task<List<Pet>> GetMyPets(int id)
        {
            return await _context.Pets.Where(p => p.OwnerId == id).ToListAsync();
        }
    }
}
