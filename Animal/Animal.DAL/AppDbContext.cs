﻿using Microsoft.EntityFrameworkCore;
using Animal.DAL.Entities;

namespace Animal.DAL
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<Specie> Species { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Specie>()
                .Property(r => r.Name)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(25);

            modelBuilder.Entity<User>()
                .Property(r => r.Email)
                .IsRequired()
                .HasMaxLength(25);
        }
    }
}
