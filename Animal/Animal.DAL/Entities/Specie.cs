﻿namespace Animal.DAL.Entities
{
    public class Specie
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
