﻿using System;

namespace Animal.DAL.Entities
{
    public class Pet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int OwnerId { get; set; }
        public virtual User Owner { get; set; }
    }
}
