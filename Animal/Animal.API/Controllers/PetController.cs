﻿using Animal.Services.RestModels.Pet;
using Animal.Services.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Animal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PetController : ControllerBase
    {
        private readonly IPetService _petService;

        public PetController(IPetService petService)
        {
            _petService = petService;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewPet([FromBody] CreateNewPetRequest request)
        {
            await _petService.CreateNewPet(request, User);
            return Created("created" ,null);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromRoute] int id)
        {
            var result = await _petService.GetById(id, User);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _petService.GetAll();
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            await _petService.DeletePet(id, User);
            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit([FromBody] UpdatePetRequest request, [FromRoute] int id)
        {
            await _petService.UpdatePet(request, id, User);
            return Ok();
        }

        [HttpGet("catfact")]
        [AllowAnonymous]
        public async Task<IActionResult> GetCatFact()
        {
            var result = await _petService.GetCatFact();
            return Ok(result);
        }
    }
}
