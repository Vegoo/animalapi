﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Animal.Services.ViewModels.User.Request;
using Animal.Services.Services.Interfaces;
using Animal.Services.RestModels.User;

namespace Animal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterUserRequest dto)
        {
            await _userService.RegisterUser(dto);
            return Ok();
        }

        [HttpPost("login")]
        public async Task<IActionResult> AuthUser([FromBody] UserLoginRequest request)
        {
            var result = await _userService.AuthUser(request);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetMe()
        {
            var result = await _userService.GetMe(User);
            return Ok(result);
        }

        [HttpGet("mypets")]
        public async Task<IActionResult> GetMyPets()
        {
            var result = await _userService.GetMyPets(User);
            return Ok(result);
        }
    }
}