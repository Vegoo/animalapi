﻿using AutoMapper;
using Animal.DAL.Entities;
using Animal.Services.ViewModels.User.Request;
using Animal.Services.ViewModels.Pet;
using Animal.Services.ViewModels.User;

namespace Animal.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, RegisterUserRequest>();

            CreateMap<User, UserFullInfoResponse>();

            CreateMap<Pet, PetResponse>();
        }
    }
}
