﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Animal.Commons.Helpers;
using Animal.DAL.Entities;
using Animal.DAL.Repositories.Interfaces;
using Animal.Services.Exceptions;
using Animal.Services.RestModels.User;
using Animal.Services.Services.Interfaces;
using Animal.Services.ViewModels.User.Respond;
using Animal.Services.ViewModels.User.Request;
using Animal.Services.ViewModels.User;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using Animal.Services.ViewModels.Pet;
using System.Collections.Generic;

namespace Animal.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IOptions<AppSettings> _appSettings;

        public UserService(IUserRepository userRepository, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _appSettings = appSettings;
        }
        public async Task RegisterUser(RegisterUserRequest request)
        {
            var userFromDb = await _userRepository.GetByEmail(request.Email);

            ValidateEmail(request.Email);
            ValidPassword(request.Password);
            if(request.DateOfBirth > DateTime.Now)
            {
                throw new BadRequestException("Invalid date");
            }
            if (userFromDb != null)
            {
                throw new BadRequestException("Invalid password or email!");
            }

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(request.Password, out passwordHash, out passwordSalt);

            var newUser = new User()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                DateOfBirth = request.DateOfBirth,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            };
            
            await _userRepository.AddUser(newUser);
        }

        public async Task<UserAuthResponse> AuthUser(UserLoginRequest request)
        {
            var userFromDb = await _userRepository.GetByEmail(request.Email);

            if (userFromDb == null)
            {
                throw new BadRequestException("Invalid password or email!");
            }

            if (!VerifyPasswordHash(request.Password, userFromDb.PasswordHash, userFromDb.PasswordSalt))
            {
                throw new BadRequestException("Invalid password or email!");
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Value.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userFromDb.Id.ToString()),
                    new Claim(ClaimTypes.Email, userFromDb.Email)
                }),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            UserAuthResponse response = new UserAuthResponse
            {
                Id = userFromDb.Id,
                Email = userFromDb.Email,
                Token = tokenString,
            };

            return response;
        }

        public async Task<UserFullInfoResponse> GetMe(ClaimsPrincipal user)
        {
            var userFromDb = await _userRepository.GetById(int.Parse(user.Identity.Name));
            var result = _mapper.Map<UserFullInfoResponse>(userFromDb);
            return result;
        }

        public async Task<List<PetResponse>> GetMyPets(ClaimsPrincipal user)
        {
            var PetsFromDb = await _userRepository.GetMyPets(int.Parse(user.Identity.Name));
            var result = _mapper.Map<List<PetResponse>>(PetsFromDb);

            return result;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void ValidateEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (!match.Success)
                throw new BadRequestException("Invalid email");
        }

        private void ValidPassword(string password)
        {
            Regex regex = new Regex(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$");
            Match match = regex.Match(password);
            if (!match.Success)
                throw new BadRequestException("Invalid password");
        }
    }     
}
