﻿using Animal.DAL.Entities;
using Animal.Services.ViewModels.Pet;
using Animal.Services.RestModels.Pet;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Animal.Services.Services.Interfaces
{
    public interface IPetService
    {
        public Task CreateNewPet(CreateNewPetRequest request, ClaimsPrincipal claimsPrincipal);
        public Task DeletePet(int Id, ClaimsPrincipal claimsPrincipal);
        public Task<PetResponse> GetById(int Id, ClaimsPrincipal claimsPrincipal);
        public Task UpdatePet(UpdatePetRequest request, int id, ClaimsPrincipal claimsIdentity);
        public Task<List<Pet>> GetAll();
        public Task<FactResponse> GetCatFact();
    }
}
