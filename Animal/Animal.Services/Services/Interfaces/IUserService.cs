﻿using Animal.Services.ViewModels.User.Request;
using Animal.Services.ViewModels.User.Respond;
using Animal.Services.RestModels.User;
using System.Threading.Tasks;
using System.Security.Claims;
using Animal.Services.ViewModels.User;
using Animal.Services.ViewModels.Pet;
using System.Collections.Generic;

namespace Animal.Services.Services.Interfaces
{
    public interface IUserService
    {
        public Task RegisterUser(RegisterUserRequest dto);
        public Task<UserAuthResponse> AuthUser(UserLoginRequest request);
        public Task<UserFullInfoResponse> GetMe(ClaimsPrincipal user);
        public Task<List<PetResponse>> GetMyPets(ClaimsPrincipal user);
    }
}
