﻿using Animal.DAL.Entities;
using Animal.DAL.Repositories.Interfaces;
using Animal.Services.ViewModels.Pet;
using Animal.Services.RestModels.Pet;
using Animal.Services.Services.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Animal.Services.Exceptions;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Animal.Services.Services
{
    public class PetService : IPetService
    {
        private readonly IPetRepository _petRepository;

        public IMapper _mapper { get; }

        public PetService(IPetRepository petRepository, IMapper mapper)
        {
            _petRepository = petRepository;
            _mapper = mapper;
        }

        public async Task CreateNewPet(CreateNewPetRequest request, ClaimsPrincipal claimsPrincipal)
        {
            var date = request.DateOfBirth;
            var shortDate = date.Value.ToString("yyyy-MM-dd");
            
            if (request.DateOfBirth > DateTime.Now)
            {
                throw new BadRequestException("Invalid date");
            }

            if (string.IsNullOrEmpty(request.Name))
            {
                throw new BadRequestException("Invalid name");
            }

            var newPet = new Pet()
            {
                Name = request.Name,
                DateOfBirth = DateTime.Parse(shortDate),
                OwnerId = int.Parse(claimsPrincipal.Identity.Name)
            };

            await _petRepository.CreateNewPet(newPet);
        }

        public async Task DeletePet(int Id, ClaimsPrincipal claimsPrincipal)
        {
            var pet = await _petRepository.GetById(Id);

            if(pet == null)
            {
                throw new NotFoundException("Pet not found");
            }

            CheckOwnerId(pet.OwnerId, int.Parse(claimsPrincipal.Identity.Name));

            if(pet.OwnerId != int.Parse(claimsPrincipal.Identity.Name))
            {
                throw new BadRequestException("Not allowed"); //change code return
            }

            _petRepository.DeletePet(pet);
        }

        public async Task<PetResponse> GetById(int Id, ClaimsPrincipal claimsPrincipal)
        {
            var result = await _petRepository.GetById(Id);
            if (result == null)
            {
                throw new NotFoundException("Pet not found");
            }
            CheckOwnerId(result.OwnerId, int.Parse(claimsPrincipal.Identity.Name));
            return _mapper.Map<PetResponse>(result);
        }

        public async Task UpdatePet(UpdatePetRequest request, int id, ClaimsPrincipal claimsPrincipal)
        {
            var pet = await _petRepository.GetById(id);
            if (pet == null)
            {
                throw new NotFoundException("Pet not found");
            }
            CheckOwnerId(pet.OwnerId, int.Parse(claimsPrincipal.Identity.Name));

            if (!string.IsNullOrEmpty(request.Name))
            {
                pet.Name = request.Name;
            }
            if (!string.IsNullOrEmpty(request.DateOfBirth.ToString()))
            {
                pet.DateOfBirth = request.DateOfBirth;
            }
            

            await _petRepository.UpdatePet();
        }

        public async Task<List<Pet>> GetAll()
        {
            var result = await _petRepository.GetAll();
            if (result == null)
            {
                throw new NotFoundException("Pet not found");
            }
            return result;
        }

        private void CheckOwnerId(int id, int claimsPrincipal)
        {
            if (id != claimsPrincipal)
            {
                throw new NotAllowedException("Not allowed");
            }
        }

        public async Task<FactResponse> GetCatFact()
        {
            RestClient client = new RestClient("https://catfact.ninja/fact");
            var restRequest = new RestRequest(Method.GET);
            IRestResponse response = await client.ExecuteAsync(restRequest);
            string factMessage = string.Empty;
            if (response.IsSuccessful)
            {
                var content = JsonConvert.DeserializeObject<JToken>(response.Content);
                factMessage = content["fact"].Value<string>();
            }
            var fact = new FactResponse()
            {
                Fact = factMessage
            };

            return fact;
        }
    }
}
