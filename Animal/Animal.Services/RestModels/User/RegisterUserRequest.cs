﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Animal.Services.ViewModels.User.Request
{
    public class RegisterUserRequest
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
