﻿using System.ComponentModel.DataAnnotations;

namespace Animal.Services.RestModels.User
{
    public class UserLoginRequest
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
