﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal.Services.RestModels.Pet
{
    public class CreateNewPetRequest
    {
        public string Name { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }
}
