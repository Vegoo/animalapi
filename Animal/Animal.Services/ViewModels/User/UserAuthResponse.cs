﻿namespace Animal.Services.ViewModels.User.Respond
{
    public class UserAuthResponse
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }
    }
}
