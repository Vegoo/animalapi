﻿using Animal.Services.ViewModels.Pet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal.Services.ViewModels.User
{
    public class UserFullInfoResponse
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public virtual List<PetResponse> Pets { get; set; }
    }
}
